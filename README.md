vehicles database
=================

**Format**: mysql table

**Data source**: world wide web


Description:
----------------

MySQL table structure and data stored in two files

schema.sql - **tbl_vehicles** table structure
data.sql - **tbl_vehicles** sql data insert


Include:
-----------

** Vehicles **: 27085

** Year range **: 1941-2009

** Properties **: 46


Available properties:
---------------------------

    Make
    Model
    Year
    Car_Category
    Car_Engine_position
    Car_Engine
    Car_Engine_type
    Car_Valves_per_cylinder
    Car_Max_power
    Car_Max_torque
    Car_Bore_stroke
    Car_Compression
    Car_Top_speed
    Car_Fuel
    Car_Transmission
    Car_Power_per_weight
    0_100_km_h_0_62_mph
    Car_Drive
    Car_Seats
    Car_Passenger_space
    Car_doors
    Car_Country_of_origin
    Car_Front_tire
    Car_Rear_tire
    Car_Chassis
    Car_CO2_emissions
    Car_Turn_circle
    Car_Weight
    Car_Towing_weight
    Car_total_length
    Car_total_width
    Car_total_height
    Car_Max_weight_with_load
    Car_Ground_clearance
    Car_Wheelbase
    Car_Cooling
    Car_Front_brakes_type
    Car_Rear_brakes_type
    Car_Cargo_space
    Car_Lubrication
    Car_front_Leg_room
    Car_Aerodynamic_dragcoefisient
    Car_Fuel_with_highway_drive
    Car_Fuel_with_mixed_drive
    Car_Fuel_with_city_drive
    Car_Fuel_tank_capacity


Usage:
----------

    $mysql -uUSER -pPASSWORD YOUDB < schema.sql
    $mysql -uUSER -pPASSWORD YOUDB < data.sql